import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import os

file_path = '56_7/postProcessing/probes/'

# List all folders in the file_path and sort them
folders = sorted([f for f in os.listdir(file_path) if os.path.isdir(os.path.join(file_path, f))])

# Create file name list
file_names = [os.path.join(file_path, folder, 'alpha.water') for folder in folders]

np_array = np.zeros((1,502))
# Print the file names
for file_name in file_names:
	print(file_name)
	# Read file starting from line 104
	with open(file_name, 'r') as file:
	    for _ in range(503):  # Skip lines 1 to 103
		next(file)
	    numbers = []
	    for line in file:
		values = line.strip().split()
		numbers.append([float(value) for value in values])

	# Convert numbers to a 2D NumPy array
	data_array = np.array(numbers)
	np_array = np.append(np_array, data_array, axis=0)

print(np_array.shape)

first_column = np_array[:,0]  # Extract the first col
remainder_columns = np_array[:,1:]  # Extract the remainder of the col
print(remainder_columns.shape)

y = np.linspace(0, 1, 501)

norm = mcolors.Normalize(vmin=0, vmax=1)

# Create a meshgrid from first_column and y
X, Y = np.meshgrid(y, first_column.ravel())
print(X.shape,Y.shape)
plt.contourf(X,Y, remainder_columns, cmap='jet', norm=norm)  # Create the contour plot
plt.colorbar()  # Add a colorbar for reference

plt.show()  # Show the plot
'''
import numpy as np

# Create the variables
var1 = np.zeros((7022, 502))
var2 = np.zeros((722, 502))

# Append var2 to var1
appended_var = np.append(var1, var2, axis=0)

# Print the shape of the appended variable
print(appended_var.shape)'''


